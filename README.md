# GetValue
**GetValue** is a small demonstration of how to use the `getValue()` method in the Java API for CPLEX to evaluate arbitrary expressions.

The test problem is a MIP model for loading pallets, with a desire to make the loads as equal to each other as possible. The model uses the L1 norm of the vector of deviations from the overall mean load. It also carries along an expression for the L2 norm of the deviations, and uses `getValue()` to evaluate both norms in two contexts: for the final solution; and, in an incumbent callback, for each feasible solution encountered.
