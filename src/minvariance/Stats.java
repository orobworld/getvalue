package minvariance;

import java.util.Arrays;
import java.util.DoubleSummaryStatistics;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Stats provides summary statistics for pallet loads, given an assignment.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Stats {
  private final double mean;     // mean load
  private double sd;             // standard deviation of load
  private final double max;      // maximum load
  private final double min;      // minimum load
  private double ssq;            // sum of squares
  private final double[] loads;  // computed loads

  /**
   * Constructor.
   * @param prob the problem instance
   * @param assign the pallet assignments
   */
  public Stats(final Problem prob, final int[] assign) {
    loads = prob.getLoads(assign);
    DoubleSummaryStatistics summary = Arrays.stream(loads).summaryStatistics();
    mean = summary.getAverage();
    min = summary.getMin();
    max = summary.getMax();
    sd = 0;
    ssq = 0;
    for (double x : loads) {
      sd += Math.pow(x - mean, 2);
      ssq += Math.pow(x, 2);
    }
    sd /= (loads.length - 1);
    sd = Math.sqrt(sd);
    // Verify that the solution is valid.
    Optional<Entry<Integer, Long>> oops =
      Arrays.stream(assign)
            .boxed()
            .collect(Collectors.groupingBy(Function.identity(),
                                           HashMap<Integer, Long>::new,
                                           Collectors.counting()))
            .entrySet()
            .stream()
            .filter(e -> e.getValue() > prob.getBinsPerPallet())
            .findFirst();
    if (oops.isPresent()) {
      Entry<Integer, Long> bad = oops.get();
      throw new IllegalArgumentException("The assignment is invalid: pallet "
                                         + bad.getKey() + " contains "
                                         + bad.getValue() + " items.");
    }
  }

  /**
   * Produces a summary of the assigned pallet loads.
   * @return a summary string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("\n\tmean = ").append(mean)
      .append("\n\tstd. dev. = ").append(sd)
      .append("\n\tmin = ").append(min)
      .append("\n\tmax = ").append(max)
      .append("\n\tssq = ").append(ssq)
      .append("\n\tloads = ").append(Arrays.toString(loads));
    return sb.toString();
  }
}
