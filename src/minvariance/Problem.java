package minvariance;

import java.util.Arrays;
import java.util.DoubleSummaryStatistics;

/**
 * Problem holds a problem instance to be solved.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Problem {
  private final int nPallets;       // number of pallets
  private final int nBins;          // number of bins
  private final int binsPerPallet;  // limit on bins per pallet
  private final double[] sizes;     // bin sizes

  /**
   * Constructor.
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public Problem() {
    // Data taken from the original question.
    nPallets = 5;
    binsPerPallet = 5;
    sizes = new double[] {
       9.810177, 22.200899, 11.896623, 11.142889, 20.142173, 17.088730,
      21.814477, 13.275619, 23.089430, 15.818683, 25.039499, 23.646295,
      26.287597, 18.422662, 19.976490, 29.095542, 21.309542, 20.272858,
      25.376852, 11.841099, 19.554247, 13.027075, 18.749811, 23.092673,
      22.104749
    };
    nBins = sizes.length;
  }

  /**
   * Gets the number of pallets.
   * @return the pallet count
   */
  public int getnPallets() {
    return nPallets;
  }

  /**
   * Gets the number of bins.
   * @return the bin count
   */
  public int getnBins() {
    return nBins;
  }

  /**
   * Gets the bin sizes.
   * @return the bin sizes
   */
  public double[] getSizes() {
    return Arrays.copyOf(sizes, nBins);
  }

  /**
   * Gets the total load.
   * @return the combined load of all bins
   */
  public double getFullLoad() {
    return Arrays.stream(sizes).sum();
  };

  /**
   * Gets the limit on the number of bins per pallet.
   * @return the pallet limit
   */
  public int getBinsPerPallet() {
    return binsPerPallet;
  }

  /**
   * Gets the pallet loads implied by an assignment of bins to pallets.
   * @param assignments a vector giving the pallet number for each bin
   * @return the pallet loads
   */
  public double[] getLoads(final int[] assignments) {
    double[] load = new double[nPallets];
    for (int i = 0; i < nBins; i++) {
      load[assignments[i]] += sizes[i];
    }
    return load;
  }

  /**
   * Gets summary statistics on the pallet loads from an assignment.
   * @param assignments the assignments
   * @return statistics on the load for each pallet
   */
  public DoubleSummaryStatistics getStats(final int[] assignments) {
    return Arrays.stream(getLoads(assignments)).summaryStatistics();
  }
}
