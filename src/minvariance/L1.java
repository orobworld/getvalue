package minvariance;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumExpr;
import ilog.concert.IloQuadNumExpr;
import ilog.cplex.IloCplex;

/**
 * L1 is a MIP model that makes assignments to minimize the L1 norm of
 * the vector of pallet loads.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class L1 extends Model {
  private final IloNumExpr l1;      // the expression for the L1 norm
  private final IloQuadNumExpr l2;  // the expression for the L2 norm

  /**
   * Constructor.
   * @param prob the problem instance
   * @param asym the type of antisymmetry constraints to use
   * @throws IloException if CPLEX cannot build the model
   */
  public L1(final Problem prob, final Asym asym) throws IloException {
    super(prob, asym);
    // Compute the mean pallet load.
    double mean = totalLoad / nPal;
    // Add constraints capturing the deviations.
    IloLinearNumExpr expr;
    for (int j = 0; j < nPal; j++) {
      expr = mip.linearNumExpr(-mean);
      expr.addTerm(1.0, p[j]);
      mip.addLe(expr, y[j], "DevHi_" + j);
      mip.addGe(expr, mip.prod(-1.0, y[j]), "DevLo_" + j);
    }
    // Minimize the sum of the deviations.
    l1 = mip.sum(y);
    mip.addMinimize(l1);
    // Set up the expression for the L2 norm, so that it can be evaluated
    // later.
    l2 = mip.quadNumExpr();
    for (int j = 0; j < nPal; j++) {
      l2.addTerm(1.0, y[j], y[j]);
    }
    // Attach an incumbent callback.
    mip.use(new IC());
  }

  /**
   * Evaluates the final solution and reports the L1 and L2 norms.
   * @return a report on the norms
   * @throws IloException if there is no final solution
   */
  public String evaluate() throws IloException {
    return String.format("%nL1 norm of the final solution = %7.4e%n",
                         mip.getValue(l1))
           + String.format("L2 norm of the final solution = %7.4e%n",
                         Math.sqrt(mip.getValue(l2)));
  }

  /**
   * IC is an incumbent callback that evaluates and reports on the L1 and
   * L2 norms of new incumbents.
   */
  private class IC extends IloCplex.IncumbentCallback {

    /**
     * Evaluates each new incumbent as it is found.
     * @throws IloException if anything upsets CPLEx
     */
    @Override
    protected void main() throws IloException {
      System.out.println(String.format("%nNew incumbent with L1 norm = %7.4e"
                                       + " and L2 norm %7.4e%n",
                                       getValue(l1), Math.sqrt(getValue(l2))));
    }
  }

}
