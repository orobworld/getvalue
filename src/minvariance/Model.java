package minvariance;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;

/**
 * Model is the parent class for all optimization models.
 * In this project, we only use one of the previously used models (L1).
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public abstract class Model {

  protected static final double ROUND = 0.5;  // rounding limit

  protected final IloCplex mip;      // the optimization model
  protected final IloNumVar[][] x;   // bin assignment variables
  protected final IloNumVar[] p;     // pallet contents
  protected final int nBin;          // number of bins
  protected final int nPal;          // number of pallets
  protected final int bpp;           // bins per pallet limit
  protected final double totalLoad;  // total load
  protected final double[] size;     // bin sizes
  protected final IloNumVar[] y;     // deviation variables

  /**
   * Constructor.
   * @param prob the problem instance
   * @param asym what (if any) measure to take to mitigate symmetry
   * @throws IloException if CPLEX cannot build the model
   */
  public Model(final Problem prob, final Asym asym) throws IloException {
    // Get key model parameters.
    nBin = prob.getnBins();
    nPal = prob.getnPallets();
    bpp = prob.getBinsPerPallet();
    totalLoad = prob.getFullLoad();
    size = prob.getSizes();
    // Instantiate the model.
    mip = new IloCplex();
    // Create the variables.
    x = new IloNumVar[nBin][nPal];
    p = new IloNumVar[nPal];
    y = new IloNumVar[nPal];
    for (int j = 0; j < nPal; j++) {
      p[j] = mip.numVar(0, totalLoad, "p_" + j);
      y[j] = mip.numVar(0, Double.MAX_VALUE, "Dev_" + j);
      for (int i = 0; i < nBin; i++) {
        x[i][j] = mip.boolVar("x_" + i + "_" + j);
      }
    }
    // Load each bin exactly once.
    for (int i = 0; i < nBin; i++) {
      mip.addEq(mip.sum(x[i]), 1.0, "LoadOnce_" + i);
    }
    // Limit the number of bins on any pallet.
    for (int j = 0; j < nPal; j++) {
      IloLinearNumExpr expr = mip.linearNumExpr();
      for (int i = 0; i < nBin; i++) {
        expr.addTerm(1.0, x[i][j]);
      }
      mip.addLe(expr, bpp, "PalletLimit_" + j);
    }
    // Define the pallet loads.
    for (int j = 0; j < nPal; j++) {
      IloLinearNumExpr expr = mip.linearNumExpr();
      for (int i = 0; i < nBin; i++) {
        expr.addTerm(size[i], x[i][j]);
      }
      mip.addEq(p[j], expr, "DefLoad_" + j);
    }
    // Deal with symmetry.
    switch (asym) {
      case LEX:
        useLex();
        break;
      case LOAD:
        useLoad();
        break;
      case DEV:
        useDev();
        break;
      case NONE:
      default:
    }
  }

  /**
   * Solves the model and return the final status.
   * @param timeLimit the time limit (in seconds) to use
   * @param seed a seed for the CPLEX random number stream
   * @return the solver status
   * @throws IloException if solution fails
   */
  public final IloCplex.Status solve(final double timeLimit, final int seed)
               throws IloException {
    // Set the random seed.
    mip.setParam(IloCplex.LongParam.RandomSeed, seed);
    // Set the time limit.
    mip.setParam(IloCplex.DoubleParam.TimeLimit, timeLimit);
    // Hold out for optimum (small differences matter).
    mip.setParam(IloCplex.Param.MIP.Tolerances.AbsMIPGap, 0);
    mip.setParam(IloCplex.Param.MIP.Tolerances.MIPGap, 0);
    // Try to solve it.
    if (mip.solve()) {
      return mip.getStatus();
    } else {
      return null;
    }
  }

  /**
   * Gets the final objective value.
   * @return the objective value
   * @throws IloException if there is no value to get
   */
  public final double getObjValue() throws IloException {
    return mip.getObjValue();
  }

  /**
   * Gets the optimal assignments.
   * @return a vector containing the pallet number for each bin.
   * @throws IloException if there is no solution
   */
  public final int[] getAssignments() throws IloException {
    int[] temp = new int[x.length];
    for (int i = 0; i < x.length; i++) {
      double[] xval = mip.getValues(x[i]);
      for (int j = 0; j < xval.length; j++) {
        if (xval[j] > ROUND) {
          temp[i] = j;
          break;
        }
      }
    }
    return temp;
  }

  /**
   * Adds constraints to force pallets to have nondecreasing loads.
   * @throws IloException if the constraints cannot be added.
   */
  private void useLoad() throws IloException {
    for (int j = 1; j < nPal; j++) {
      mip.addLe(p[j - 1], p[j], "Asym_" + j);
    }
  }

  /**
   * Adds constraints to force pallets to have lexically nondecreasing
   * assignments.
   * @throws IloException if the constraints cannot be added.
   */
  private void useLex() throws IloException {
    for (int i = 1; i < nBin; i++) {
      for (int j = 1; j < nPal; j++) {
        int j0 = j - 1;
        IloLinearNumExpr expr = mip.linearNumExpr();
        for (int i0 = 0; i0 < i; i0++) {
          expr.addTerm(1.0, x[i0][j0]);
        }
        mip.addLe(x[i][j], expr, "Asym_" + i + "_" + j);
      }
    }
  }

  /**
   * Adds constraints forcing pallets to have nondecreasing deviations from
   * mean.
   * @throws IloException if the constraints cannot be added.
   */
  private void useDev() throws IloException {
    for (int j = 1; j < nPal; j++) {
      mip.addLe(y[j - 1], y[j], "Asym_" + j);
    }
  }
}
