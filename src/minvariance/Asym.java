package minvariance;

/**
 * Asym enumerates options for mitigating symmetry.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public enum Asym {
  /** NONE: do nothing about symmetry. */
  NONE,
  /** LOAD: make pallets have nondecreasing loads. */
  LOAD,
  /** LEX: lexicograpically order pallets. */
  LEX,
  /** DEV: make pallets have nondecreasing deviations. */
  DEV
}
