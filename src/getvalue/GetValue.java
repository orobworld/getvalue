package getvalue;

import ilog.concert.IloException;
import ilog.cplex.IloCplex;
import java.util.Arrays;
import minvariance.Asym;
import minvariance.L1;
import minvariance.Problem;
import minvariance.Stats;

/**
 * GetValue uses a problem posted by Erwin Kalvelagen at
 * https://yetanothermathprogrammingconsultant.blogspot.com/2017/09/minimizing-standard-deviation.html.
 * (assigning bins to pallets) to demonstrate the use of IloCplex.getValue() and
 * IloCplex.IncumbentCallback.getValue(), applied to arbitrary expressions.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class GetValue {
  private static final int SEED = 92117;

  /**
   * Dummy constructor.
   */
  private GetValue() { }

  /**
   * Runs the demonstration.
   * @param args the command line arguments (not used)
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    // Create the problem instance.
    Problem problem = new Problem();
    // Instantiate and solve the L1 model.
    System.out.println("\nL1 model.\n=====");
    try {
      L1 mod = new L1(problem, Asym.NONE);
      IloCplex.Status status = mod.solve(120., SEED);
      System.out.println("Solver status = " + status);
      if (status == IloCplex.Status.Optimal
          || status == IloCplex.Status.Feasible) {
        System.out.println("\nFinal objective value = " + mod.getObjValue());
      }
      if (status == IloCplex.Status.Optimal) {
        int[] x = mod.getAssignments();
        System.out.println("\nOptimal pallet assignments:\n"
                           + Arrays.toString(x));
        System.out.println("\nPallet loads:\n"
                           + Arrays.toString(problem.getLoads(x)));
        System.out.println("\nStatistics:" + (new Stats(problem, x)));
        System.out.println(mod.evaluate());
      }
    } catch (IloException ex) {
      System.out.println("Could not solve the L1 model:\n"
                         + ex.getMessage());
    }
  }

}
